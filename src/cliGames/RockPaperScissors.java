package cliGames;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

	private enum gameEnum {
		ROCK, PAPER, SCISSORS
	};

	private String gameValues[] = { "ROCK", "PAPER", "SCISSORS" };
	private final String winnerPlayer = "Player win";
	private final String winnerComputer = "Computer win";
	private static int playerWinCounter = 0;
	private static int computerWinCounter = 0;

	public static void main(String[] args) {
		RockPaperScissors game = new RockPaperScissors();
		game.playRockPaperScissors();
		System.out.println("Thanks for playing game\nYou win " + playerWinCounter + " times" + "\nComputer win "
				+ computerWinCounter + " times");
	}

	private void playRockPaperScissors() {
		int max = 3;
		Scanner sc = new Scanner(System.in);
		char choice;
		int computerChoice;
		String computerValue;
		int playerChoice;
		String playerValue;
		String winnerIs;
		String status = null;
		boolean keepPlaying = false;

		do {
			showMenu();
			System.out
					.println("\nChoose one \n" + "1)Rock \n2)Paper \n3)Scissors \nEnter related number to that object");

			playerChoice = sc.nextInt();

			if (playerChoice > max) {
				System.err.println("Error wrong choice");
				continue;
			}
			computerChoice = generateRandom(max);

			System.out.println("You choosed : " + convertChoice(playerChoice));
			System.out.println("Computer choosed : " + convertChoice(computerChoice));

			status = checkWinner(convertChoice(playerChoice), convertChoice(computerChoice));
			playerWinCounter += (status.equals(winnerPlayer) ? 1 : 0);
			computerWinCounter += (status.equals(winnerComputer) ? 1 : 0);
			System.out.println("Game status : " + status);

			System.out.println("\nYou want to play again? (Y/N)");
			choice = sc.next().charAt(0);

			if (choice == 'Y' || choice == 'y')
				keepPlaying = true;
			else
				keepPlaying = false;

		} while (keepPlaying);
	}

	private void showMenu() {
		System.out.println("Rock Paper scissors is game for 2 to 3 Players");
		System.out.println("Rock could defeat scissors but not Paper");
		System.out.println("Paper could defeat rock but not Scissors");
		System.out.println("Scissors could defeat paper but not Rock");
	}

	private String convertChoice(int num) {
		return gameValues[num - 1];
	}

	private String checkWinner(String player, String computer) {
		String winner = null;

		if (player.equals(gameValues[gameEnum.ROCK.ordinal()]) && computer.equals(gameValues[gameEnum.PAPER.ordinal()]))
			winner = winnerComputer;

		else if (player.equals(gameValues[gameEnum.ROCK.ordinal()])
				&& computer.equals(gameValues[gameEnum.SCISSORS.ordinal()]))
			winner = winnerPlayer;

		else if (player.equals(gameValues[gameEnum.PAPER.ordinal()])
				&& computer.equals(gameValues[gameEnum.ROCK.ordinal()]))
			winner = winnerPlayer;

		else if (player.equals(gameValues[gameEnum.PAPER.ordinal()])
				&& computer.equals(gameValues[gameEnum.SCISSORS.ordinal()]))
			winner = winnerComputer;

		else if (player.equals(gameValues[gameEnum.SCISSORS.ordinal()])
				&& computer.equals(gameValues[gameEnum.PAPER.ordinal()]))
			winner = winnerPlayer;

		else if (player.equals(gameValues[gameEnum.SCISSORS.ordinal()])
				&& computer.equals(gameValues[gameEnum.ROCK.ordinal()]))
			winner = winnerComputer;

		else
			winner = "draw";

		return winner;
	}

	private int generateRandom(int high) {
		Random random = new Random();
		int low = 1;
		return random.nextInt(high - low) + low;
	}
}