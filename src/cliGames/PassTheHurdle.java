package cliGames;
import java.util.Random;
import java.util.Scanner;

/*Pass the Hurdle Game*/
public class PassTheHurdle {
	
	//variables to hold player's information.
	private static int player1Location = 0;
	private static int player2Location = 0;
	private static int destinationMiles = 100;
	private static int penalty = 10;
	private static int diceResult = 0;
	private static int diceNumberSix = 6;
	private static int[] hurdlesLocation = new int[10];

	public static void main(String[] args) {
		char inputChoice = 'N';
		Scanner sc = new Scanner(System.in);

		// print Game description.
		System.out.println("Pass the Hurdle Game\r\n" + "\r\n"
				+ "INFO : this game is played by two players consisting of one dice \r\n"
				+ "MISSION : you have to reach your destination which is 100 miles away from your location\n"
				+ "and in every 10 miles there is hurdle which is a penalty and you have to pass it\n"
				+ "and if you will hit penalty then 10 miles will be detucted from your current location,\r\n"
				+ "BONUS :  if you get six number on dice you get a new turn to play.\r\n" + "\r\n"
				+ "GOAL : The first person to reach destination wins. Good luck!\n");

		// initialize hurdles location in array.
		System.out.print("Hurdles at locations : [");
		try {
			for (int i = 0; i < hurdlesLocation.length; i++) {
				hurdlesLocation[i] = (i + 1) * 8 + i;
				System.out.print(hurdlesLocation[i] + ",");
			}
			System.out.println("]\n");
		} catch (ArrayIndexOutOfBoundsException ex) {
			System.out.println("Array length exception : " + ex.getLocalizedMessage());
		}
		
		/*Loop to re-start game*/
		do {
			playGame();
			System.out.println("Play PassTheHurdle game again (Y/N)");

			inputChoice = sc.next().charAt(0);
		} while (inputChoice == 'y' || inputChoice == 'Y');

		System.out.println("Thanks for playing PassTheHurdle game...");
	}
	
	/*Method that initiates gameplay*/
	private static void playGame() {
		char playerInput;
		Scanner sc = new Scanner(System.in);
		
		/*Reset player information if game re-starts*/
		player1Location = 0;player2Location = 0;diceResult = 0;

		System.out.println("Player1's current location is : " + player1Location);
		System.out.println("Player2's current location is : " + player2Location);

		// Continue loop until destination not reached.
		while (true) {

			/* Player 1 rolling section */
			System.out.println("Player 1's turn, type 'R' to roll dice");
			playerInput = sc.next().charAt(0);
			player1Location = rollPlayerTurn(player1Location, 1);

			/*Checking if player 1 reached destination */
			if(player1Location > destinationMiles) {
				System.out.println("Player 1 won the game :)");
				break;
			}
			
			/* Checking dice roll number six for Player 1 */
			if (diceResult == diceNumberSix) {
				System.out.println("Wohooo ! you got a new turn! type 'R' to roll again");
				continue;
			}
			 

			/* Player 2 rolling section */
			System.out.println("Player 2's turn, type 'R' to roll dice");
			playerInput = sc.next().charAt(0);
			player2Location = rollPlayerTurn(player2Location, 2);
			
			/*Checking if player 2 reached destination */
			if(player2Location > destinationMiles) {
				System.out.println("Player 2 won the game :)");
				break;
			}

			/* Checking dice roll number six for Player 2 */
			while(diceResult == diceNumberSix) {
				
				System.out.println("Wohooo ! you got a new turn, Type 'R' to roll again");
				playerInput = sc.next().charAt(0);
				player2Location =  rollPlayerTurn(player2Location,2);
				
				/*Checking if player 2 reached destination */
				if(player2Location > destinationMiles) {
					System.out.println("Player 2 won the game :)");
					return; /*breaks from nested-inner loop*/
				}
			}
		}
	}
	
	/*Roll current player's turn in action */
	private static int rollPlayerTurn(int playerLocation, int playerID) {

		/* Player section , dice roll and hurdle checking */
		diceResult = rollDice();
		System.out.println("Player " + playerID + " rolled " + diceResult +  " on dice");
		playerLocation += diceResult;

		System.out.println("Player " + playerID + " location is : " + playerLocation + "\n");

		/* Check for Hurdles for Player */
		if (checkForHurdle(playerLocation)) {
			playerLocation -= penalty;

			/* Rounding location to 0th place if negative */
			if (playerLocation < 0)
				playerLocation = 0;

			System.out.println("Ooops you hit the hurdle ! your location now is " + playerLocation);
		}

		return playerLocation;
	}

	/*Method to check for any hurdles in between destination */
	private static boolean checkForHurdle(int playerPosition) {
		for (int i = 0; i < hurdlesLocation.length; i++) {
			if (playerPosition == hurdlesLocation[i])
				return true;
		}
		return false;
	}
	
	/*Method to roll Dice */
	private static int rollDice() {
		Random random = new Random();
		int Low = 1;
		int High = diceNumberSix + 1;
		int rand = random.nextInt(High - Low) + Low;
		return rand;
	}

}
