package cliGames;
import java.util.Random;
import java.util.Scanner;

public class GuessGame {

	private Scanner sc;

	public static void main(String[] args) {
		GuessGame obj = new GuessGame();
		obj.startMethod();
	}

	private void startMethod() {

		System.out.println("Welcome to Guess Game\nEnter Number to Guess");
		int guessCount = randomGuess((readInteger()));
		System.out.println("It took you : " + guessCount + " tries to guess");
	}

	private int readInteger() {
		int num;
		sc = new Scanner(System.in);
		num = sc.nextInt();
		return num;
	}

	private int randomGuess(int inp) {
		int input = inp;
		int guessCount = 0;
		int randMaxLimit = 100;
		int rand = generateRandom(randMaxLimit);

		while (input != rand) {

			if (input < rand) {
				System.out.println("Guess too low");
				guessCount++;
			}

			else if (input > rand) {
				System.out.println("Guess too High");
				guessCount++;
			}

			else if (input == rand) {
				break;
			}

			input = readInteger();
		}

		System.out.println("Guess is correct number was : " + rand);
		return guessCount;
	}


	private int generateRandom(int randMaxLimit) {
		Random random = new Random();
		int Low = 0;
		int High = randMaxLimit;
		int rand = random.nextInt(High - Low) + Low;
		return rand;
	}

}
